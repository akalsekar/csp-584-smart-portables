import java.io.*;
import java.sql.*;
import java.io.IOException;
import java.util.*;

public class ProductRecommenderUtility{
	
	static Connection conn = null;
    static String message;
	
	public void getConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/SmartPortable?useSSL=false", "root", "Akash@1993");
		} catch (Exception e) {
			//System.out.println("Abjsjs");
			//e.printStackTrace();
			
		}
	}

	public HashMap<String,String> readOutputFile(){

		String TOMCAT_HOME = System.getProperty("catalina.home");
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
		HashMap<String,String> prodRecmMap = new HashMap<String,String>();
		try {

            br = new BufferedReader(new FileReader(new File("C:/apache-tomcat-7.0.34/webapps/csj/DealMatch/output.csv")));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] prod_recm = line.split(cvsSplitBy,2);
				prodRecmMap.put(prod_recm[0],prod_recm[1]);
            }
			
		} catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
		}
		
		return prodRecmMap;
	}
	
	public Product getProduct(String product)
	{
		Product sp = null;
		java.sql.Statement stm = null;
		java.sql.Statement sm = null;
		ArrayList<Product> products = new ArrayList<Product>();
		//ArrayList<Accessory> access = new ArrayList<Accessory>();
		String brandName, model,imageName,type,accessories;
		int productId, quantity;
		float price, discount,rebate;
		try
		{
			getConnection();
			String selectProd="select * from  productdetails where model=?";
			PreparedStatement pst = conn.prepareStatement(selectProd);
			pst.setString(1,product);
			ResultSet rst = pst.executeQuery();
			 while(rst.next()){

                sp = new Product();
				sp.setProductId(rst.getInt(1));
                sp.setBrandName(rst.getString(2));
				sp.setModel(rst.getString(3));
                sp.setPrice(rst.getFloat(4));
				sp.setDiscount(rst.getFloat(5));
				sp.setRebate(rst.getFloat(6));
                sp.setQuantity(rst.getInt(7));
				sp.setImageName(rst.getString(8));
				sp.setType(rst.getString(9));
				String accsStr = rst.getString(10);
		
		//products.add(sp);
		}	
			} 
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sp;
	}
}