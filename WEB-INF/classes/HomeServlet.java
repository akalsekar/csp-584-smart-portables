import java.util.Map;

import javax.servlet.*;

import javax.servlet.http.*;

import java.io.*;

import java.util.logging.*;

import java.util.*;


public class HomeServlet extends HttpServlet {
	
	ArrayList<Product> products= new ArrayList<>();
		SaxParserUtility sp = new SaxParserUtility("C:/apache-tomcat-7.0.34/webapps/csj/xml/ProductCatalog.xml");

	public void init()
	{
		try{
		System.out.println("Sending request to SAXParser");	
		sp.loadDataStore();
		}
		catch(Exception E)
		{
			System.out.println("Exception");
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException
	
	{
		HttpSession session = request.getSession(true);
				
		String userInfo = new String("UserInfo");
				
		Person person = null;
				
		int cartItemCount=0;
				
		String fname="";

				
		if (null != session.getAttribute(userInfo)) 
		{
					
			person = (Person) session.getAttribute(userInfo);
					
			fname = person.getFName();
			
		}

	
		Cart newCart = new Cart();
			   
		ArrayList<Cart> cartList = new ArrayList<Cart>();

		   
		String cartInSession = new String("Cart"); 
			   
		if(null!=session.getAttribute(cartInSession))
		{
					 
		cartList = (ArrayList<Cart>)session.getAttribute(cartInSession);
			  
		  }

				
		cartItemCount= cartList.size();

			
		response.setContentType("text/html");

				
		Utilities utility = new Utilities();
				
		PrintWriter out = response.getWriter();
				
		utility.printHtml("C:/apache-tomcat-7.0.34/webapps/csj/Header.html", response);

		if (person !=null)
		{
		out.println("<nav>");	
		out.println("<ul  class='nav'>");
		out.println("<li><a href='CartServlet'>Cart(" + cartItemCount+ ")<span class='glyphicon glyphicon-shopping-cart'></span></a><li> ");

		out.println("<li><a href='LogoutServlet'>Logout</a><li>"); 
		  if (person != null && person.getRole().equalsIgnoreCase("StoreManager"))
			{
				
				out.println("<li>");
					out.println("<div class='dropdown'>");
						out.println("<a class='btn btn-secondary dropdown-toggle' role='button' type='button' data-toggle='dropdown'>Welcome, " + fname + "&#9786");
						out.println("</a>");
						out.println("<ul class='dropdown-menu' style='padding-top: 0px;padding-bottom: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;'>");
							out.println("<li><a class='dropdown-item' href='AddProducts'>Add Products</a></li>");
							out.println("<li><a class='dropdown-item' href='UpdateProduct'>Update Products</a></li>");
							out.println("<li><a class='dropdown-item' href='DeleteProducts'>DeleteProducts</a></li>");
						out.println("</a>");
						out.println("</ul>");
					out.println("</div>");
				out.println("</li>");
				out.println("<li>");
					out.println("<div class='dropdown'>");
						out.println("<a class='btn btn-secondary dropdown-toggle' role='button' type='button' data-toggle='dropdown'>Inventory");
						out.println("</a>");
						out.println("<ul class='dropdown-menu' style='padding-top: 0px;padding-bottom: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;'>");
							out.println("<li><a class='dropdown-item' href='Inventory'>Available Products</a></li>");
							out.println("<li><a class='dropdown-item' href='SalesProducts'>Products On Sale</a></li>");
							out.println("<li><a class='dropdown-item' href='RebateProducts'>Manufacturer Rebates</a></li>");
						out.println("</a>");
						out.println("</ul>");
					out.println("</div>");
				out.println("</li>");
				out.println("<li>");
					out.println("<div class='dropdown'>");
						out.println("<a class='btn btn-secondary dropdown-toggle' role='button' type='button' data-toggle='dropdown'>Sales Report");
						out.println("</a>");
						out.println("<ul class='dropdown-menu' style='padding-top: 0px;padding-bottom: 0px;border-right-width: 0px;border-top-width: 0px;border-bottom-width: 0px;'>");
							out.println("<li><a class='dropdown-item' href='SalesReport'>Products Sold</a></li>");
							out.println("<li><a class='dropdown-item' href='DailySales'>Daily Sales</a></li>");
						out.println("</a>");
						out.println("</ul>");
					out.println("</div>");
				out.println("</li>");
				/*out.println("<li><a>Welcome, " + fname + "&#9786</a> ");
				out.println("<ul class='navh'>");	      	
				out.println("<li><a href='AddProducts'>Add Products</a></li>");			
				out.println("<li><a href='UpdateProduct'>Update Products</a></li>");
				out.println("<li><a href='DeleteProducts'>Delete Products</a></li>");
				out.println("<li class='dropdown'>");
				out.println("<a class='dropbtn'>Inventory</a>");
				out.println("<div class='dropdown-content'>");
				out.println("<a href='Inventory'>Available Products</a>");
				out.println("<a href='SalesProducts'>Products On Sale</a>");
				out.println("<a href='RebateProducts'>Manufacturer Rebates</a>");
				out.println("</div>");
				out.println("</li>");

				out.println("<li class='dropdown'>");
				out.println("<a class='dropbtn'>Sales Report</a>");
				out.println("<div class='dropdown-content'>");
				out.println("<a href='SalesReport'>Products Sold</a>");
				out.println("<a href='DailySales'>Daily Sales</a>");
				out.println("</div>");
				out.println("</li>");

				out.println("</ul");
				out.println("</li>");*/
				//out.println("</ul>");
				out.println("</ul>");
				out.println("</nav>");
			}        
		  if (person != null && person.getRole().equalsIgnoreCase("Customer"))
			{
				out.println("<ul class='nav'>"); 
				out.println("<li><a>Welcome, " + fname + "&#9786</a> ");   
			}
				
		if (person != null && person.getRole().equalsIgnoreCase("SalesMan")) 
		{
			
			out.println("<ul class='nav'>");
			out.println("<li><a style='padding:21px 15px'>Welcome, " + fname + "&#9786</a> ");	
			out.println("<ul>");		
			out.println("<li><a href='UserRegistrationServlet'>Create Customer Account</a></li>");
			out.println("<li><a href='SalesmanOperations'>Add Customer Orders</a></li>");
						
			out.println("<li><a href='Salesman_Up_CustOrder'>Update Customer Orders</a></li>");
						
			out.println("<li><a href='Salesman_Up_CustOrder'>Delete Customer Orders</a></li>");
			out.println("</ul");
			out.println("</li>");
			out.println("</ul>");
			out.println("</ul>");
			out.println("</nav>");		
		}
		out.println("</ul>");
		 
		out.println("</nav>");

		}
		utility.printHtml("C:/apache-tomcat-7.0.34/webapps/csj/Content.html",response);

				
		utility.printHtml("C:/apache-tomcat-7.0.34/webapps/csj/LeftNavigationBar.html", response);		
		utility.printHtml("C:/apache-tomcat-7.0.34/webapps/csj/Footer.html", response);
		
	}

}
